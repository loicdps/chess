package edu.uqac.aop.chess;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

import edu.uqac.aop.chess.agent.Move;

public aspect LogAspect {
	pointcut move(): call(Move agent.Player.makeMove());
	
    after() returning(Object r) : move(){	

    	BufferedWriter bw = null;
        File f = new File("./log.txt");

        try {
        	if (f.exists()) {
        		bw = new BufferedWriter(new FileWriter(f, true));
        	} else {
                bw = new BufferedWriter(new FileWriter(f));
        	}
            if (f.exists()) bw.newLine();
            bw.write("Coup : " + r.toString());
            bw.flush();
        } catch (FileNotFoundException ex) {
            System.out.println(ex.toString());
        } catch (IOException e) {
        	e.printStackTrace();
        }
    }
}
