package edu.uqac.aop.chess;

import edu.uqac.aop.chess.agent.Move;
import edu.uqac.aop.chess.agent.Player;

public aspect MoveValidator {
	
	pointcut move(Move mv, Board playGround, Player player) : call(boolean agent.Player.makeMove(Move, Board)) && args(mv, playGround) && target(player);
	
	Object around(Move mv, Board playGround, Player player): move(mv, playGround, player){

		//V�rifie si c'est une pi�ce et pas une case vide
		if(!playGround.getGrid()[mv.xI][mv.yI].isOccupied()) { //Si pas de pi�ce
			System.out.println("Vous n'avez pas s�lectionn� de pi�ce.");
			return false;
		}
		
		//V�rifie si la pi�ce appartient au joueur
		if(playGround.getGrid()[mv.xI][mv.yI].getPiece().getPlayer() == player.getColor()) { //
			System.out.println("Cette pi�ce ne vous appartient pas.");
			return false;
		}
		
		//V�rifie si le mouvement est possible
		if(!playGround.getGrid()[mv.xI][mv.yI].getPiece().isMoveLegal(mv)) {
			System.out.println("Ce mouvement n'est pas possible.");
			return false;
		} else {
			//V�rifie si la pi�ce reste dans l'�chiquier
			if((mv.xF < 0 || mv.xF >= 8) || (mv.yF < 0 || mv.yF >= 8)) {
				System.out.println("Ce mouvement va en dehors de l'�chiquier.");
				return false;
			}
		}
		
		//V�rifie si la pi�ce ne prend pas une autre pi�ce du joueur
		if(playGround.getGrid()[mv.xF][mv.yF].isOccupied()) {
			if(playGround.getGrid()[mv.xF][mv.yF].getPiece().getPlayer() != player.getColor()){
				System.out.println("Vous ne pouvez pas prendre votre propre pi�ce.");
				return false;
			}
		}
		
		//V�rifie si la pi�ce saute d'autres pi�ces (sauf knight, pawn, king)
		//rook, queen straight
		if(playGround.getGrid()[mv.xI][mv.yI].getPiece().toString().equalsIgnoreCase("T") || playGround.getGrid()[mv.xI][mv.yI].getPiece().toString().equalsIgnoreCase("D")) {
			if(mv.yI < mv.yF && mv.xI == mv.xF) {
				for(int yStart = mv.yI + 1; yStart < mv.yF ; yStart++) { //axe y+ (vertical)
					if(playGround.getGrid()[mv.xF][yStart].isOccupied()) {
						System.out.println("Vous ne pouvez pas traverser d'autre pi�ce.");
						return false;
					}
				}
			}
			if(mv.xI < mv.xF && mv.yI == mv.yF) {
				for(int xStart = mv.xI + 1; xStart < mv.xF ; xStart++) { //axe x+ (horizontal)
					if(playGround.getGrid()[xStart][mv.yF].isOccupied()) {
						System.out.println("Vous ne pouvez pas traverser d'autre pi�ce.");
						return false;
					}
				}
			}
			if(mv.yI > mv.yF && mv.xI == mv.xF) {
				for(int yStart = mv.yI - 1; yStart > mv.yF ; yStart--) { //axe y+ (vertical)
					if(playGround.getGrid()[mv.xF][yStart].isOccupied()) {
						System.out.println("Vous ne pouvez pas traverser d'autre pi�ce.");
						return false;
					}
				}
			}
			if(mv.xI > mv.xF && mv.yI == mv.yF) {
				for(int xStart = mv.xI - 1; xStart > mv.xF ; xStart--) { //axe x+ (horizontal)
					if(playGround.getGrid()[xStart][mv.yF].isOccupied()) {
						System.out.println("Vous ne pouvez pas traverser d'autre pi�ce.");
						return false;
					}
				}
			}
		}
		
		//bishop, queen diagonal
		if(playGround.getGrid()[mv.xI][mv.yI].getPiece().toString().equalsIgnoreCase("F") || playGround.getGrid()[mv.xI][mv.yI].getPiece().toString().equalsIgnoreCase("D")) {
			if(mv.yI < mv.yF && mv.xI < mv.xF) { //bas droite
				int xStart = mv.xI;
				for(int yStart = mv.yI + 1; yStart < mv.yF ; yStart++) {
					xStart++;
					if(playGround.getGrid()[xStart][yStart].isOccupied()) {
						System.out.println("Vous ne pouvez pas traverser d'autre pi�ce (BD).");
						return false;
					}
				}
			}
			if(mv.yI < mv.yF && mv.xI > mv.xF) { //bas gauche
				int xStart = mv.xI;
				for(int yStart = mv.yI + 1; yStart < mv.yF ; yStart++) {
					xStart--;
					if(playGround.getGrid()[xStart][yStart].isOccupied()) {
						System.out.println("Vous ne pouvez pas traverser d'autre pi�ce (BG).");
						return false;
					}
				}
			}
			if(mv.yI > mv.yF && mv.xI < mv.xF) { //haut droite
				int xStart = mv.xI;
				for(int yStart = mv.yI - 1; yStart > mv.yF ; yStart--) {
					xStart++;
					if(playGround.getGrid()[xStart][yStart].isOccupied()) {
						System.out.println("Vous ne pouvez pas traverser d'autre pi�ce (HD).");
						return false;
					}
				}
			}
			if(mv.yI > mv.yF && mv.xI > mv.xF) { //haut gauche
				int yStart = mv.yI;
				for(int xStart = mv.xI - 1; xStart > mv.xF ; xStart--) {
					yStart--;
					if(playGround.getGrid()[xStart][yStart].isOccupied()) {
						System.out.println("Vous ne pouvez pas traverser d'autre pi�ce (HG).");
						return false;
					}
				}
			}
		}	
		
		playGround.movePiece(mv);
		return true;
	}
}
